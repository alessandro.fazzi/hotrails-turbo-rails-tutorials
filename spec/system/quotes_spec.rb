require 'system_helper'

RSpec.describe 'Quotes', type: :system do
  fixtures :quotes

  context 'Within quotes CRUD' do
    it 'creates a quote' do
      visit quotes_path
      within('h1') do
        expect(page).to have_content('Quotes')
      end

      click_on 'New quote'
      within('h1') do
        expect(page).to have_content('New quote')
      end

      fill_in 'Name', with: 'Capybara quote'
      click_on 'Create Quote'
      within('h1') do
        expect(page).to have_content('Quote')
      end
      expect(page).to have_content('Capybara quote')
    end

    context 'having a quote in DB' do
      let(:quote) { quotes(:first) }

      it 'shows a quote' do
        visit quotes_path
        click_on quote.name

        within 'h1' do
          expect(page).to have_content(quote.name)
        end
      end

      it 'updates a quote' do
        visit quotes_path
        within 'h1' do
          expect(page).to have_content('Quotes')
        end

        click_on 'Edit', match: :first
        within 'h1' do
          expect(page).to have_content('Edit quote')
        end

        fill_in 'Name', with: 'Updated quote'
        click_on 'Update Quote'

        within 'h1' do
          expect(page).to have_content('Quotes')
        end
        expect(page).to have_content('Updated quote')
      end

      it 'destroys a quote' do
        visit quotes_path
        expect(page).to have_content(quote.name)

        click_on 'Delete', match: :first
        expect(page).to_not have_content(quote.name)
      end
    end
  end
end
